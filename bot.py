# -*- coding: utf-8 -*-

'''            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
					Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

			DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
'''
from types import SimpleNamespace
import os
import os.path
import re
import sys
import logging

import yaml
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import telegram

from sphinx import Sphinx

class SphinxBot:
	def __init__(self, config):
		self.config = SimpleNamespace(**config)
		self.updater = Updater(config['token'])
		self.dispatcher = self.updater.dispatcher

		self.dispatcher.add_handler(CommandHandler('start', self.start))
		self.dispatcher.add_handler(CommandHandler('riddle', self.riddle))
		self.dispatcher.add_handler(MessageHandler(Filters.text, self.text))

		self.sphinx = sphinx = Sphinx(config)

	def start_polling(self):
		self.updater.start_polling()

	def start(self, bot, update):
		if update.message.chat.type != 'private':
			return

		update.message.reply_text(self.config.greeting)

	def text(self, bot, update):
		if update.message.chat.type != 'private':
			return

		try:
			answer = self.sphinx.answer(update.message.from_user.id, update.message.text)
		except Exception as e:
			log.debug('Unexpected error: {}'.format(sys.exc_info()))

		update.message.reply_text(text=str(answer),
									  parse_mode=telegram.ParseMode.MARKDOWN)

	def riddle(self, bot, update):
		if update.message.chat.type != 'private':
			return

		try:
			answer = self.sphinx.riddle(update.message.from_user.id)
		except Exception as e:
			log.debug('Unexpected error: {}'.format(sys.exc_info()))

		update.message.reply_text(text=str(answer),
									  parse_mode=telegram.ParseMode.MARKDOWN)

def main(config):
	bot = SphinxBot(config)
	bot.start_polling()

if __name__ == '__main__':
	with open(os.path.expanduser('config.yml'), 'r') as f:
		try:
			config = yaml.load(f)
		except yaml.YAMLError:
			print('incorrect format')
			exit(1)

	log = logging.getLogger('bot')
	logger = logging.StreamHandler(sys.stdout)
	logger.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
	logger.setLevel(logging.DEBUG)
	log.addHandler(logger)
	log.setLevel(logging.DEBUG)

	main(config)