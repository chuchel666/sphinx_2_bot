# -*- coding: utf-8 -*-

'''            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
'''

class UserNotFoundResponse():
	def __init__(self, user_id):
		self.__user_id = user_id

	def __str__(self):
		return "user {} not found".format(self.__user_id)

class CooldownResponse():
	def __init__(self, answer):
		self.__answer = answer

	def __str__(self):
		return self.__answer

class NoRiddlesResponse():
	def __str__(self):
		return "no riddles to answer found for user"

class CorrectAnswerResponse():
	def __init__(self, reply_text):
		self.__reply_text = reply_text

	def __str__(self):
		return self.__reply_text

class IncorrectAnswerResponse():
	def __init__(self, reply_text):
		self.__reply_text = reply_text

	def __str__(self):
		return self.__reply_text

class RiddleResponse():
	def __init__(self, riddle):
		self.__riddle = riddle

	def __str__(self):
		return self.__riddle

class UserIsBannedResponse():
	def __init__(self, reply_text):
		self.__reply_text = reply_text

	def __str__(self):
		return self.__reply_text

class AdminRequiredResponse():
	def __init__(self, reply_text):
		self.__reply_text = reply_text

	def __str__(self):
		return self.__reply_text