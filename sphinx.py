# -*- coding: utf-8 -*-

'''            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
'''

import os
import datetime
import random

from types import SimpleNamespace

import yaml

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, BigInteger, Boolean, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.orm import sessionmaker

from responses import *

Base = declarative_base()

class Riddle(Base):
	__tablename__ = 'riddles'

	id = Column(Integer, primary_key=True)
	riddle_text = Column(String)
	answers = Column(String)

	def __str__(self):
		return "id: {}, riddle_text: {}, answers: {}".format(self.id, self.riddle_text, self.answers)

class UserRiddle(Base):
	__tablename__ = 'user_riddles'

	id = Column(Integer, primary_key=True)
	user_id = Column(BigInteger, ForeignKey('users.id'), nullable=False)
	riddle_id = Column(Integer, ForeignKey('riddles.id'), nullable=False)
	riddle = relationship("Riddle")
	answers = relationship('UserAnswer', backref='answer', lazy=False)
	date = Column(DateTime, nullable=False)

class User(Base):
	__tablename__ = 'users'

	id = Column(BigInteger, primary_key=True)
	banned = Column(Boolean)
	admin = Column(Boolean)
	cooldown = Column(DateTime)
	riddle_counter = Column(Integer)
	riddles = relationship('UserRiddle', backref='user', lazy=False)

class UserAnswer(Base):
	__tablename__ = 'user_answers'

	id = Column(Integer, primary_key=True)
	user_riddle_id = Column(Integer, ForeignKey('user_riddles.id'), nullable=False)
	user_riddle = relationship("UserRiddle")
	answer_text = Column(String)
	correct = Column(Boolean)
	date = Column(DateTime)

class Sphinx():
	def __init__(self, config):
		self.config = SimpleNamespace(**config)

		self.sql_engine = create_engine('sqlite:///'+self.config.dbpath, echo=True)

		self.Session = sessionmaker(bind = self.sql_engine)

		Base.metadata.create_all(self.sql_engine)

		self.load_riddles(config['riddles'])

	def load_riddles(self, riddles):
		session = self.Session()

		for r in riddles:
			riddle = Riddle(riddle_text=r["riddle"], answers=",".join(r["answers"]))
			session.add(riddle)

		session.commit()
		session.close()

	def get_all_riddles(self):
		session = self.Session()

		result = session.query(Riddle).all()

		session.close()

		return result

	def get_random_riddle(self):
		session = self.Session()
		# cannot use GROUP BY func.random here because of sqlite
		riddles = session.query(Riddle).filter().all()

		session.close()

		return random.choice(riddles)

	def riddle(self, user_id):
		session = self.Session()

		user = session.query(User).get(user_id)

		if user is None:
			user = User(id=user_id, banned=False, admin=False, riddle_counter=0)

		if user.banned:
			session.close()
			return UserIsBannedResponse(self.config.banned_answer)

		if self._process_max_attempts(session, user):
			session.close()
			return CooldownResponse(self.config.cooldown_answer)


		if user.cooldown is not None and datetime.timedelta(user.cooldown.second, datetime.datetime.now().second).seconds <= self.config.cooldown_seconds:
			session.close()
			return CooldownResponse(self.config.cooldown_answer)

		user.cooldown = None

		riddle = self._set_riddle(session, user)

		session.close()

		return RiddleResponse(riddle.riddle_text)

	def answer(self, user_id, text):
		session = self.Session()

		user = session.query(User).get(user_id)

		if user is None:
			session.close()
			return UserNotFoundResponse(user_id)

		if user.banned:
			session.close()
			return UserIsBannedResponse(self.config.banned_answer)

		if user.riddles is None or len(user.riddles) == 0:
			session.close()
			return NoRiddlesResponse()

		if self._process_max_attempts(session, user):
			session.close()
			return CooldownResponse(self.config.cooldown_answer)

		if user.cooldown is not None and datetime.timedelta(user.cooldown.second, datetime.datetime.now().second).seconds <= self.config.cooldown_seconds:
			session.close()
			return CooldownResponse(self.config.cooldown_answer)

		user.cooldown = None

		user_riddle = user.riddles[-1]
		answers = user_riddle.riddle.answers.split(",")

		answer = UserAnswer(user_riddle_id=user_riddle.riddle.id, answer_text=text, date=datetime.datetime.now())

		session.add(user)

		if text in answers:
			correct = True
			reply = CorrectAnswerResponse(self.config.correct_answer)
		else:
			correct = False
			reply = IncorrectAnswerResponse(self.config.incorrect_answer)

		answer.correct = correct

		session.add(answer)
		session.commit()
		session.close()

		return reply

	def ban(self, user_id):
		session = self.Session()

		user = session.query(User).get(user_id)

		if user is None:
			session.close()
			return UserNotFoundResponse(user_id)

		if not (user.admin or user_id in self.config.admins):
			session.close()
			return AdminRequiredResponse(self.config.admin_required_answer)

		user.banned = True

		session.add(user)
		session.commit()
		session.close()

	def unban(self, user_id):
		session = self.Session()

		user = session.query(User).get(user_id)

		if user is None:
			session.close()
			return UserNotFoundResponse(user_id)

		if not (user.admin or user_id in self.config.admins):
			session.close()
			return AdminRequiredResponse(self.config.admin_required_answer)

		user.banned = False

		session.add(user)
		session.commit()
		session.close()


	def _process_max_attempts(self, session, user):
		if user.riddle_counter >= self.config.max_attempts:
			user.cooldown = datetime.datetime.now()
			user.riddle_counter = 0
			session.add(user)
			session.commit()

			return True
		else:
			user.riddle_counter += 1
			session.add(user)
			session.commit()

			return False

	def _set_riddle(self, session, user):
		riddle = self.get_random_riddle()
		user_riddle = UserRiddle(user_id=user.id, riddle_id=riddle.id, date=datetime.datetime.now())
		user.riddles.append(user_riddle)
		
		session.add(user_riddle)
		session.add(user)

		session.commit()

		return riddle

def main():
	with open(os.path.expanduser('config.yml'), 'r') as f:
		try:
			config = yaml.load(f)
		except yaml.YAMLError:
			print('incorrect format')
			exit(1)
	sphinx = Sphinx(config)
	sphinx.test()

if __name__ == "__main__":
	main()