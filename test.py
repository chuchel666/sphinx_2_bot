# -*- coding: utf-8 -*-

'''            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
'''

import unittest
import os
import time

import yaml

from sphinx import Sphinx

from responses import *

class TestSphinxCore(unittest.TestCase):

	def test_load_riddles(self):
		if os.path.exists("test.db"):
			os.remove("test.db")

		config = {
			"dbpath" : "test.db", 
			"riddles" : 
				[
					{
						"riddle" : "путин", 
						"answers" : ["хуйло", "вор"]
					}
				]
		}

		sphinx = Sphinx(config)

		riddles = sphinx.get_all_riddles()

		for r in riddles:
			print(r)

		self.assertEqual(len(riddles), 1)
		self.assertTrue(riddles[0].riddle_text == "путин") 
		self.assertTrue(riddles[0].answers == "хуйло,вор")

	def test_random_riddle(self):
		if os.path.exists("test.db"):
			os.remove("test.db")

		config = {
			"dbpath" : "test.db", 
			"riddles" : 
				[
					{
						"riddle" : "путин", 
						"answers" : ["хуйло", "вор"]
					},
					{
						"riddle" : "где",
						"answers" : ["в пизде"]
					},
					{
						"riddle" : "кто",
						"answers" : ["ты"]
					}
				]
		}

		sphinx = Sphinx(config)

		riddle = sphinx.get_random_riddle()
		print(riddle)
		self.assertIsNotNone(riddle)

	def test_riddle(self):
		if os.path.exists("test.db"):
			os.remove("test.db")

		USER_ID = 1

		config = {
			"dbpath" : "test.db", 
			"cooldown_answer" : "хуй",
			"cooldown_seconds" : 4,
			"max_attempts" : 3,
			"riddles" : 
				[
					{
						"riddle" : "путин", 
						"answers" : ["хуйло", "вор"]
					},
					{
						"riddle" : "где",
						"answers" : ["в пизде"]
					},
					{
						"riddle" : "кто",
						"answers" : ["ты"]
					}
				]
		}

		sphinx = Sphinx(config)

		for r in range(0, config['max_attempts']):
			answer = sphinx.riddle(USER_ID)
			print(answer)
			self.assertIsNotNone(answer)
			self.assertNotEqual(answer, config['cooldown_answer'])

		answer = sphinx.riddle(USER_ID)
		self.assertIs(type(answer), CooldownResponse)
		self.assertEqual(str(answer), config['cooldown_answer'])

		answer = sphinx.riddle(2)
		self.assertIs(type(answer), RiddleResponse)
		self.assertNotEqual(str(answer), config['cooldown_answer'])

		time.sleep(5)

		answer = sphinx.riddle(USER_ID)
		self.assertIs(type(answer), RiddleResponse)
		self.assertNotEqual(str(answer), config['cooldown_answer'])

	def test_answers(self):
		if os.path.exists("test.db"):
			os.remove("test.db")

		USER_ID = 1

		config = {
			"dbpath" : "test.db", 
			"cooldown_answer" : "хуй",
			"correct_answer" : "круто",
			"incorrect_answer" : "хуйня",
			"cooldown_seconds" : 5,
			"max_attempts" : 4,
			"riddles" : 
				[
					{
						"riddle" : "путин", 
						"answers" : ["хуйло", "вор"]
					}
				]
		}

		sphinx = Sphinx(config)

		sphinx.riddle(USER_ID)

		answer = sphinx.answer(USER_ID, "хуйло")
		self.assertIs(type(answer), CorrectAnswerResponse)
		self.assertEqual(str(answer), config['correct_answer'])

		answer = sphinx.answer(USER_ID, "вор")
		self.assertIs(type(answer), CorrectAnswerResponse)
		self.assertEqual(str(answer), config['correct_answer'])

		answer = sphinx.answer(USER_ID, "не хуйло")
		self.assertIs(type(answer), IncorrectAnswerResponse)
		self.assertEqual(str(answer), config['incorrect_answer'])

		answer = sphinx.answer(USER_ID, "не хуйло")
		self.assertIs(type(answer), CooldownResponse)
		self.assertEqual(str(answer), config['cooldown_answer'])

		riddle = sphinx.riddle(2)
		self.assertIs(type(riddle), RiddleResponse)
		self.assertEqual(str(riddle), "путин")
		
		answer = sphinx.answer(2, "хуйло")
		self.assertIs(type(answer), CorrectAnswerResponse)
		self.assertEqual(str(answer), config['correct_answer'])

		time.sleep(5)

		answer = sphinx.answer(USER_ID, "не хуйло")
		self.assertIs(type(answer), IncorrectAnswerResponse)
		self.assertEqual(str(answer), config['incorrect_answer'])

		answer = sphinx.answer(USER_ID, "хуйло")
		self.assertIs(type(answer), CorrectAnswerResponse)
		self.assertEqual(str(answer), config['correct_answer'])

	def test_ban(self):
		if os.path.exists("test.db"):
			os.remove("test.db")

		USER_ID = 1

		config = {
			"dbpath" : "test.db", 
			"cooldown_answer" : "хуй",
			"correct_answer" : "круто",
			"incorrect_answer" : "хуйня",
			"banned_answer" : "хуй тебе",
			"admin_required_answer" : "нужен админ",
			"cooldown_seconds" : 5,
			"admins" : [USER_ID],
			"max_attempts" : 4,
			"riddles" : 
				[
					{
						"riddle" : "путин", 
						"answers" : ["хуйло", "вор"]
					}
				]
		}

		sphinx = Sphinx(config)

		sphinx.riddle(USER_ID)

		answer = sphinx.ban(USER_ID)
		self.assertIsNone(answer)

		answer = sphinx.ban(666)
		self.assertIs(type(answer), UserNotFoundResponse)

		answer = sphinx.riddle(USER_ID)
		self.assertIs(type(answer), UserIsBannedResponse)
		self.assertEqual(str(answer), config['banned_answer'])

		answer = sphinx.answer(USER_ID, "хуй")
		self.assertIs(type(answer), UserIsBannedResponse)
		self.assertEqual(str(answer), config['banned_answer'])

		answer = sphinx.unban(USER_ID)
		self.assertIsNone(answer)

		answer = sphinx.riddle(USER_ID)
		self.assertIs(type(answer), RiddleResponse)

		answer = sphinx.answer(USER_ID, "хуйло")
		self.assertIs(type(answer), CorrectAnswerResponse)

		answer = sphinx.unban(666)
		self.assertIs(type(answer), UserNotFoundResponse)

		sphinx.riddle(777)

		answer = sphinx.ban(777)
		self.assertIs(type(answer), AdminRequiredResponse)
		self.assertEqual(str(answer), config['admin_required_answer'])

		answer = sphinx.unban(777)
		self.assertIs(type(answer), AdminRequiredResponse)
		self.assertEqual(str(answer), config['admin_required_answer'])



if __name__ == "__main__":
	unittest.main()